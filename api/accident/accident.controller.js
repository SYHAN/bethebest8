'use strict';
const mysql = require('mysql');
const commUtil = require('../../common/util');
const provider = require('./accident.provider');
const co = require('co');
const config = require('config');

module.exports.list = function(req,res){
  console.log('++++++++++++++++++++++controller');
  provider.list(req.query)
   .then(commUtil.responseSuccess(res))
   .catch(commUtil.responseError(res));
}
