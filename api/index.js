'use strict';

const express = require('express');
const router = express.Router();

var accidentRouter = require('./accident');

router.use('/accident',accidentRouter);

module.exports = router;
