'use strict';
const express = require('express');
const router = express.Router();
const restRouter = require('../api');
const commUtil = require('../common/util');
const cookieParser = require('cookie-parser');
const config = require('config');
const db = require('../common/db');
var util = require('util');
/**  페이지 라우팅 시 crowd 인증여부 체크
    인증되지 않은 경우 : crowd cookie 없음 , 유효한 cookie가 아님
**/


router.use('/api',restRouter);

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Bethebest8' });
});

router.get('/test_03', function(req, res, next) {
  res.render('test_03', { title: 'Bethebest8' });
});

router.get('/3dchart', function(req, res, next) {
  res.render('3dchart', { title: 'Bethebest8' });
});




module.exports = router;
