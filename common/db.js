'use strict';
const mysql = require("mysql");
const config = require("config");
const co = require('co');
const thunkify = require('thunkify');

const db = mysql.createPool(config.mysqlConfig);
module.exports.db = db;

module.exports.query = co.wrap(function *(sqlStatement,params){

  const connection = yield getConnection();
  const rows = yield execQuery(connection,sqlStatement,params);
  connection.release();
  console.log(rows);
  return rows;

});

function execQuery(connection, sqlStatement, params) {
  return new Promise((resolve, reject) => {
    connection.query(sqlStatement, params, function (error, rows) {
      if (error) {
        connection.release();
        return reject(error);
      }
      resolve(rows);
      console.log(rows);
    });
  });
}
function getConnection() {
  return new Promise((resolve, reject) => {
    db.getConnection((error, connection) => {
      if (error) {
        console.log(error);
        return reject(error);
      }
      console.log('=======================connection');
      resolve(connection);
    });
  });
}
