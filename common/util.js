'use strict';
const co = require('co');
const config = require('config');
const fs = require('fs');

//  json data 를  query 로 변환
module.exports.jsonToQueryString = function(json,delimiter,useQuotation) {
    return Object.keys(json).map(function(key) {
            if(useQuotation == true){
              return key + '= "' + json[key] + '"';
              console.log(key + '= "' + json[key] + '"');
            }
            else
              return key + '=' + json[key];
        }).join(delimiter);
}

module.exports.queryStringToJSON = function(query) {
    var pairs = query.slice(1).split('&');

    var result = {};
    pairs.forEach(function(pair) {
        pair = pair.split('=');
        result[pair[0]] = decodeURIComponent(pair[1] || '');
    });

    return JSON.parse(JSON.stringify(result));
}

module.exports.responseSuccess = function (res) {

  return co.wrap(function * (result) {

    res.status(200).json({
      items: result
     });
  });
}

module.exports.responseError = function (res) {
  return co.wrap(function * (error) {
    console.log('==============error' + error);
    res.status(500).json({
      message: error.errorMessages?error.errorMessages:error.message,
      stack: error.stack
    })
  });
};

module.exports.getIp = function(ip){
  if (ip.length < 15)
  {
     ip = ip;
  }
  else
  {
     var nyIP = ip.slice(7);
     ip = nyIP;
  }
  return ip;
}
